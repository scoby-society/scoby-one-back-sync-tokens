FROM node:16.14.2-buster-slim

RUN apt update

WORKDIR /app

COPY package*.json .

RUN npm ci

COPY . .

CMD ["node", "src/app.js"]