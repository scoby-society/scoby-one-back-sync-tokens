const axios = require('axios');

const ApiSolana = axios.create({
  baseURL: apiSolana,
  headers: {
    'Content-Type': 'application/json',
  },
});

let TRX_SOL = [];
let TRANFER_SPORE = [];
let id_temp = 1;

const saveSporeDsTrx = async () => {
  // let values = []
  // for (let spore_trx = 0; spore_trx < TRANFER_SPORE.length; spore_trx++) {
  //     let trx = TRANFER_SPORE[spore_trx]
  //     const query = "insert into spore_ds_trx" +
  //         "(id , spore_id , event, price , from_wallet," +
  //         "to_wallet , signature , instructions , date_trx)" +
  //         "values ($1,$2,$3,$4,$5,$6,$7,$8,$9);"

  //     values = [
  //         id_temp,
  //         trx.spore_id,
  //         trx.event,
  //         trx.price,
  //         trx.from_wallet,
  //         trx.to_wallet,
  //         trx.signature,
  //         trx.instructions,
  //         trx.date_trx
  //     ]

  //     let response = await pool.query(query, values);
  //     id_temp += 1
  // }
  TRANFER_SPORE = [];
};

const sync_trx_db = async () => {
  let temp_array = [];
  let temp_spores = [];

  try {
    const query = 'SELECT * FROM spore_ds_trx;';
    const rows = await process.postgresql.query(query);
    if (rows.length != 0) {
      rows.forEach((element) => {
        temp_array.push(element.signature);
      });

      for (let item = 0; item < TRANFER_SPORE.length; item++) {
        let trx = TRANFER_SPORE[item];
        if (temp_array.indexOf(trx.signature) == -1) {
          temp_spores.push({
            spore_id: trx.spore_id,
            event: trx.event,
            price: trx.price,
            from_wallet: trx.from_wallet,
            to_wallet: trx.to_wallet,
            signature: trx.signature,
            instructions: trx.instructions,
            date_trx: trx.date_trx,
          });
        }
      }

      TRANFER_SPORE = temp_spores;
    } //end if
  } catch (error) {}
};

const getinfoTrx = async (id_spore, trxs) => {
  for (let trx_id = 0; trx_id < trxs.length; trx_id++) {
    let trx = trxs[trx_id];
    try {
      const response = await ApiSolana(`/transaction/${trx.txHash}`);
      let value = response.data.tokenTransfers;
      let size = response.data.tokenTransfers.length;
      if (size != 0) {
        const date = new Date(trx.blockTime * 1000).toISOString();
        TRANFER_SPORE.push({
          spore_id: id_spore,
          signature: trx.txHash,
          date_trx: date,
          from_wallet: value[0].source_owner,
          to_wallet: value[0].destination_owner,
          instructions: '',
          event: 'Spl-Transfer',
          price: 0,
        });
      } // if tranfer
    } catch (error) {} // end try-catch
  } // end trx for

  //get_mint
  try {
    mint_hash = trxs[trxs.length - 1];
    const response = await ApiSolana(`/transaction/${mint_hash.txHash}`);
    const date = new Date(mint_hash.blockTime * 1000).toISOString();
    let value = response.data.unknownTransfers[0];
    TRANFER_SPORE.push({
      spore_id: id_spore,
      signature: mint_hash.txHash,
      date_trx: date,
      from_wallet: '',
      to_wallet: value['event'][0].source,
      instructions: '',
      event: 'Mint',
      price: 0,
    });
  } catch (e) {}

  if (TRANFER_SPORE.length != 0) {
    try {
      await saveSporeDsTrx();
    } catch (e) {
      console.log(e);
    }
  }
};

const spore_trx = async (spores) => {
  let beforeHash = '';
  let is_spores = true;
  for (let item = 0; item < spores.length; item++) {
    let spore = spores[item];
    while (is_spores) {
      const response = await ApiSolana('/account/transactions', {
        params: {
          account: spore.contract_adress,
          beforeHash: beforeHash,
          limit: 50,
        },
      }); //end ApiSolana
      if (response.data.length == 0 || response == undefined) is_spores = false;
      else {
        beforeHash = response.data[response.data.length - 1].txHash;
        if (TRX_SOL.length != 0) TRX_SOL = TRX_SOL.concat(response.data);
        else TRX_SOL = response.data;
      } // response.data.length > 1
    } //end while
    await getinfoTrx(spore.id, TRX_SOL);
    console.log(`spore_id : ${item}`);
    array_trx = [];
    is_spores = true;
    beforeHash = '';
    TRX_SOL = [];
  } //end for
};

module.exports = spore_trx;
