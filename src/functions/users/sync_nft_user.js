//get DB
const { PoolDB, GetNftCollection } = require('../../db/index');

const get_id_user = async (address) => {
  const query = `SELECT user_id FROM spore_ds WHERE wallet_adress='${address}';`;
  const rows = await process.postgresql.query(query);
  return rows;
};

const sync_user_nft = async () => {
  let nft_collections = [],
    nft_user_null = [],
    nft_orphans = [],
    nft_finaly = [],
    save_nfts = [],
    collections = [],
    values = [];

  // Get nfts save in DB
  collections = await PoolDB();
  collections.push({
    title: 'Royalty',
    pool: '237izm5xHkaTVFXa76L9pLq2XyV4yJCF9LA3cCD5mBpf',
    wallet_adress: '7zP1BjWppJNpVj1ioxpKE9SaejxSismd8J7d3ortEcNn',
    symbol: 'RLTY',
  });

  for (let i = 0; i < collections.length; i++)
    nft_collections.push(GetNftCollection(collections[i].symbol));

  nft_collections = await Promise.all(nft_collections);

  for (let i = 0; i < nft_collections.length; i++) {
    let nft_finaly = nft_collections[i];
    for (let k = 0; k < nft_finaly.length; k++) {
      if (nft_finaly[k].user_id == null) {
        nft_user_null.push(nft_finaly[k]);
      }
    }
  }
  let users_id = [];
  for (let i = 0; i < nft_user_null.length; i++) {
    users_id = await get_id_user(nft_user_null[i].wallet_adress);
    for (let k = 0; k < users_id.length; k++) {
      if (users_id[k].user_id != null) {
        nft_user_null[i].user_id = users_id[k].user_id;
      }
    }
    users_id = [];
  }

  for (let i = 0; i < nft_user_null.length; i++) {
    const user_id = nft_user_null[i].user_id ? `"${nft_user_null[i].user_id}"` : null;
    const query = `UPDATE spore_ds SET "user_id"=${user_id} WHERE "id"=${nft_user_null[i].id}`;
    await process.postgresql.query(query);
  }
};

module.exports = sync_user_nft;
