const {
  Users,
  SocialNetworkByUserID,
  NftTransactions,
} = require('../../../db');

const PoolData = [
  {
    title: 'Magic Spores',
    pool: '4QawWnjkTW91g3Qvh3cz39C9xQ4VQB6s181XB6xAe6hY',
    wallet_adress: 'CzCCZkzxGRaic54jmaMezmLxSudyQPTfRKvkVzet264f',
    symbol: '',
  },
  {
    title: 'Crypto Hills Club NFP',
    pool: 'Fp92aPit322vMts1NDuow1ZLmTDknuAUHqeQC4PmRFGT',
    wallet_adress: 'AMbQxtDNMxrMkUVb5fDaBKQFutvTS2SerfhG7DBgokaN',
    symbol: '',
  },
  {
    title: 'Royalty',
    pool: '237izm5xHkaTVFXa76L9pLq2XyV4yJCF9LA3cCD5mBpf',
    wallet_adress: '7zP1BjWppJNpVj1ioxpKE9SaejxSismd8J7d3ortEcNn',
    symbol: '',
  },
  {
    title: 'Zombie VS Infected',
    pool: 'GeCRaiFKTbFzBV1UWWFZHBd7kKcCDXZK61QvFpFLen66',
    wallet_adress: 'CGgz52xaPTzaTpb74Ja3ZA52xhRedE58d1xMAZ9FkwBM',
    symbol: '',
  },
  {
    title: 'vs Infected',
    pool: 'GeCRaiFKTbFzBV1UWWFZHBd7kKcCDXZK61QvFpFLen66',
    wallet_adress: 'CGgz52xaPTzaTpb74Ja3ZA52xhRedE58d1xMAZ9FkwBM',
    symbol: '',
  },
];

/*
    Example table
    total_contribution = double
    as_creator = double
    as_collector = double
    as_connector = double
    invitations_request = integer
    invitations_send = integer
    user_contribudor = integer referencia a id usuario
    source_user =  integer referencia a id usuario
*/

/*
    1 - scoby
    2 - creator 
    3 - creator scout ->  person who invited the Creator / Persona que invitó al Creador(masculino)
    4 - Collector Scout -> person who invited the collector who minted or traded
    5 - Connector Scout -> [the person who invited the scout of the collector who minted or traded)
*/
/*
 {
    source_user: 14,
    user_contribudor: 49,
    as_creator: 0.1740445,
    as_collector: 0.3729525,
    as_connector: 0.1243175,
    total_contribution: 0.6713144999999999
  },

*/

const saveMembersDB = async (metricsMembers) => {
  for (let i = 0; i < metricsMembers.length; i++) {
    const {
      source_user = 'NULL',
      user_contribudor = 'NULL',
      as_creator = 'NULL',
      as_collector = 'NULL',
      as_connector = 'NULL',
      total_contribution = 'NULL',
      invitations_requested = 'NULL',
      invitation_sent = 'NULL',
    } = metricsMembers[i];
    const query = `INSERT INTO member_network (source_user, user_contribudor, as_creator, as_collector, as_connector, total_contribution, invitations_request, invitations_send) VALUES (${source_user}, ${user_contribudor}, ${as_creator}, ${as_collector}, ${as_connector}, ${total_contribution}, ${invitations_requested}, ${invitation_sent});`;
    await process.postgresql.query(query);
  }
};

const getInvitationInfo = async (metricsMembers) => {
  for (let i = 0; i < metricsMembers.length; i++) {
    socialNetwork = await SocialNetworkByUserID(
      metricsMembers[i].user_contribudor
    );
    metricsMembers[i].invitations_requested =
      socialNetwork[0].invitations_requested;
    metricsMembers[i].invitation_sent = socialNetwork[0].invitation_sent;
  }
  return metricsMembers;
};

const getTotalcontribution = async (metricsMembers) => {
  for (let i = 0; i < metricsMembers.length; i++) {
    if (metricsMembers[i].total_contribution == null)
      metricsMembers[i].total_contribution = 0;

    if (metricsMembers[i]?.as_creator)
      metricsMembers[i].total_contribution += metricsMembers[i].as_creator;
    if (metricsMembers[i]?.as_collector)
      metricsMembers[i].total_contribution += metricsMembers[i].as_collector;
    if (metricsMembers[i]?.as_connector)
      metricsMembers[i].total_contribution += metricsMembers[i].as_connector;
  }
  return metricsMembers;
};

const getAsConnector = async (users, metricsMembers) => {
  let transactionNFT = {},
    user_contribudor,
    poolTransactions,
    transaction;

  for (let i = 0; i < PoolData.length; i++) {
    poolTransactions = await NftTransactions(PoolData[i].title);
    for (let k = 0; k < poolTransactions.length; k++) {
      transaction = poolTransactions[k];
      sourceUser = users.find(
        (user) => user.public_key == transaction.wallet_connector_scout
      );

      user_contribudor = users.find(
        (user) => user.public_key == transaction.wallet_connector_scout_source
      );

      if (user_contribudor) {
        metricMember = metricsMembers.find(
          (member) =>
            member.user_contribudor == user_contribudor.id &&
            member.source_user == sourceUser.id
        );

        if (metricMember) {
          if (metricMember.as_connector == null) metricMember.as_connector = 0;

          if (transaction.connector_scout != null) {
            metricMember.as_connector += parseFloat(
              transaction.connector_scout
            );
          } else {
            metricMember.as_connector += 0;
          }
        } else {
          if (transaction.connector_scout != null) {
            metricsMembers.push({
              source_user: sourceUser.id,
              user_contribudor: user_contribudor.id,
              as_connector: parseFloat(transaction?.connector_scout),
            });
          } else {
            metricsMembers.push({
              source_user: sourceUser.id,
              user_contribudor: user_contribudor.id,
              as_connector: 0,
            });
          }
        }
      } // end user_contribudor
    }
  } // for PoolData
  return metricsMembers;
};

const getAsCollector = async (users, metricsMembers) => {
  let transactionNFT = {},
    user_contribudor,
    poolTransactions,
    transaction;

  for (let i = 0; i < PoolData.length; i++) {
    poolTransactions = await NftTransactions(PoolData[i].title);
    for (let k = 0; k < poolTransactions.length; k++) {
      transaction = poolTransactions[k];
      sourceUser = users.find(
        (user) => user.public_key == transaction.wallet_collector_scout
      );

      user_contribudor = users.find(
        (user) => user.public_key == transaction.wallet_collector_scout_source
      );

      if (user_contribudor) {
        metricMember = metricsMembers.find(
          (member) =>
            member.user_contribudor == user_contribudor.id &&
            member.source_user == sourceUser.id
        );

        if (metricMember) {
          if (metricMember.as_collector == null) metricMember.as_collector = 0;
          if (transaction.collector_scout != null) {
            metricMember.as_collector += parseFloat(
              transaction.collector_scout
            );
          } else {
            metricMember.as_collector += 0;
          }
        } else {
          if (transaction.collector_scout != null) {
            metricsMembers.push({
              source_user: sourceUser.id,
              user_contribudor: user_contribudor.id,
              as_collector: parseFloat(transaction.collector_scout),
            });
          } else {
            metricsMembers.push({
              source_user: sourceUser.id,
              user_contribudor: user_contribudor.id,
              as_collector: 0,
            });
          }
        }
      } // end user_contribudor
    }
  } // for PoolData
  return metricsMembers;
};

const getAsCreator = async (users, metricsMembers) => {
  let transactionNFT = {},
    user_contribudor,
    poolTransactions,
    transaction;

  for (let i = 0; i < PoolData.length; i++) {
    poolTransactions = await NftTransactions(PoolData[i].title);
    for (let k = 0; k < poolTransactions.length; k++) {
      transaction = poolTransactions[k];
      sourceUser = users.find(
        (user) => user.public_key == transaction.wallet_creator_scout
      );

      user_contribudor = users.find(
        (user) => user.public_key == transaction.wallet_creator_scout_source
      );

      if (user_contribudor && sourceUser) {
        metricMember = metricsMembers.find(
          (member) =>
            member.user_contribudor == user_contribudor.id &&
            member.source_user == sourceUser.id
        );

        if (metricMember) {
          if (metricMember.as_creator == null) metricMember.as_creator = 0;

          if (transaction.creator_scout != null) {
            metricMember.as_creator += parseFloat(transaction.creator_scout);
          } else {
            metricMember.as_creator += 0;
          }
        } else {
          if (transaction.creator_scout != null) {
            metricsMembers.push({
              source_user: sourceUser.id,
              user_contribudor: user_contribudor.id,
              as_creator: parseFloat(transaction?.creator_scout),
            });
          } else {
            metricsMembers.push({
              source_user: sourceUser.id,
              user_contribudor: user_contribudor.id,
              as_creator: 0,
            });
          }
        }
      } // end user_contribudor
    }
  } // for PoolData
  return metricsMembers;
};

// 49 roger 479 aldo 14 moto 476 cesar
const MembersMetric = async () => {
  let users = [],
    NftTransactions = [],
    metricsMembers = [];

  users = await Users();

  // as creator
  metricsMembers = await getAsCreator(users, metricsMembers);

  //as collector
  metricsMembers = await getAsCollector(users, metricsMembers);

  //as connector
  metricsMembers = await getAsConnector(users, metricsMembers);

  //total_contribution
  metricsMembers = await getTotalcontribution(metricsMembers);

  // get invitation
  metricsMembers = await getInvitationInfo(metricsMembers);

  console.log(metricsMembers);
  //await saveMembersDB(metricsMembers)
};

module.exports = MembersMetric;
