const { Activitys, RequestInvitation } = require('../../../db');

const nftTRX = require('./creator_royatils');

/*
    getAllActivitys  : Get all activities of the platform registered in the database
    getAllRequestInvitation : Get invitations in the new web version
*/

// const ARRAYS_EVENTS = [
//     'follow', 'session', 'message',
//     'joinRequest', 'userRequestInvitation'
// ]

const ARRAYS_EVENTS = ['userRequestInvitation'];

const getAllActivitys = async () => {
  let activitys_db = [],
    invitacions;

  invitacions = await Activitys();
  for (let i = 0; i < ARRAYS_EVENTS.length; i++) {
    activitys_db.push(
      invitacions.filter((element) => {
        return element.type_action == ARRAYS_EVENTS[i];
      })
    );
  }
  return activitys_db;
};

/*
    Nombre de la tabla:"RequestInvitation"
    id
    sourceUser:number
    targetUser:number
    createdAt:Date
*/
const getAllRequestInvitation = async () => {
  let activitys_db = [],
    invitacions;

  invitacions = await RequestInvitation();
  // for (let i = 0; i < ARRAYS_EVENTS.length; i++) {
  //     activitys_db.push(
  //         invitacions.filter(
  //             element => {
  //                 return element.type_action == ARRAYS_EVENTS[i]
  //             }
  //         )
  //     )
  // }
  return activitys_db;
};

module.exports = {
  getAllActivitys,
  nftTRX,
};
