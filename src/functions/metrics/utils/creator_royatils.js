const axios = require('axios');
const { PoolDB } = require('../../../db');

const { NftTransactions } = require('../../../db');

let TRX_SOL = [];
let TRANFER_NFT = [];
let id_temp = 1;

const ApiSolana = axios.create({
  baseURL: 'https://public-api.solscan.io',
  headers: {
    'Content-Type': 'application/json',
  },
});

let PoolData = [];

var TEMP_TRX = [];
var CollectionTile = '';

const savenftDsTrx = async () => {
  let values = [];
  const pool = new Pool(config);
  console.log("\nTRX Save : " + TRANFER_NFT.length)
  for (let nft_trx = 0; nft_trx < TRANFER_NFT.length; nft_trx++) {
    let trx = TRANFER_NFT[nft_trx];
    const {
      nft_address = 'NULL',
      signature = 'NULL',
      date_trx = 'NULL',
      price = 'NULL',
      from_wallet = 'NULL',
      toWallet = 'NULL',
      instructions = 'NULL',
      event = 'NULL',
      scoby = 'NULL',
      wallet_scoby = 'NULL',
      wallet_scoby_source = 'NULL',
      creator_scout = 'NULL',
      wallet_creator_scout = 'NULL',
      wallet_creator_scout_source = 'NULL',
      collector_scout = 'NULL',
      wallet_collector_scout = 'NULL',
      wallet_collector_scout_source = 'NULL',
      connector_scout = 'NULL',
      wallet_connector_scout = 'NULL',
      wallet_connector_scout_source = 'NULL',
      creator = 'NULL',
      wallet_creator = 'NULL',
      wallet_creator_source = 'NULL',
    } = trx;
    const query = `INSERT INTO nfts_transactions (nft_address, signature, date_trx, price, from_wallet, toWallet, instructions, event, scoby, wallet_scoby, wallet_scoby_source, creator_scout, wallet_creator_scout,wallet_creator_scout_source, collector_scout, wallet_collector_scout, wallet_collector_scout_source, connector_scout, wallet_connector_scout, wallet_connector_scout_source, source_collection, creator, wallet_creator, wallet_creator_source) VALUES (${nft_address}, ${signature}, ${date_trx}, ${price}, ${from_wallet}, ${toWallet}, ${instructions}, ${event}, ${scoby}, ${wallet_scoby}, ${wallet_scoby_source}, ${creator_scout}, ${wallet_creator_scout},${wallet_creator_scout_source}, ${collector_scout}, ${wallet_collector_scout}, ${wallet_collector_scout_source}, ${connector_scout}, ${wallet_connector_scout}, ${wallet_connector_scout_source}, ${CollectionTile}, ${creator}, ${wallet_creator}, ${wallet_creator_source});`;
    await process.postgresql.query(query);
  }

  TRANFER_NFT = [];
};

//Delete trx exists in of db
const deleteTrxExists = async () => {
  let trx_sync = [];
  let nftTransactionsDB = await NftTransactions(CollectionTile);

  nftTransactionsDB.forEach((element) => {
    trx_sync.push(element.signature);
  });

  nftTransactionsDB = [];
  for (let i = 0; i < TRANFER_NFT.length; i++) {
    if (trx_sync.indexOf(TRANFER_NFT[i].signature) == -1) {
      nftTransactionsDB.push(TRANFER_NFT[i]);
    }
  }
  TRANFER_NFT = nftTransactionsDB;
};

const getRoyatilsAndDestination = async (
  serialNumber,
  walletUser,
  nameCollection,
  unknownTransfers
) => {
  MAIN_ACTIONS = [];
  let validatorRoyalties = 0;
  let creatorMintNFT = false;
  let poolInfo = PoolData.find((element) => element.title == nameCollection);
  if (poolInfo.wallet_adress == walletUser) {
    creatorMintNFT = true;
  }
  for (let i = 0; i < unknownTransfers.length; i++) {
    if (unknownTransfers[i].event?.length) {
      size = unknownTransfers[i].event.length - 2;
      for (let event = 0; event < size; event++) {
        if (unknownTransfers[i].event[event].type == 'transfer') {
          amount = unknownTransfers[i].event[event].amount / 1000000000;
          source = unknownTransfers[i].event[event].source;
          destination = unknownTransfers[i].event[event].destination;

          if (validatorRoyalties == 0) {
            // royalties scoby
            MAIN_ACTIONS.push({
              scoby: amount,
              wallet_scoby: destination,
              wallet_scoby_source: source,
            });
          } else if (validatorRoyalties == 1) {
            //royalties creator
            if (creatorMintNFT == true) {
              MAIN_ACTIONS.push({
                creator_scout: amount,
                wallet_creator_scout: destination,
                wallet_creator_scout_source: source,
              });
            } else {
              MAIN_ACTIONS.push({
                creator: amount,
                wallet_creator: destination,
                wallet_creator_source: source,
              });
            }
          } else if (validatorRoyalties == 2) {
            //creator Scout
            MAIN_ACTIONS.push({
              creator_scout: amount,
              wallet_creator_scout: destination,
              wallet_creator_scout_source: source,
            });
          } else if (validatorRoyalties == 3) {
            // collector scout
            MAIN_ACTIONS.push({
              collector_scout: amount,
              wallet_collector_scout: destination,
              wallet_collector_scout_source: source,
            });
          } else if (validatorRoyalties == 4) {
            // connector scout
            MAIN_ACTIONS.push({
              connector_scout: amount,
              wallet_connector_scout: destination,
              wallet_connector_scout_source: source,
            });
          }
        } // end if transfer

        if (creatorMintNFT == true && validatorRoyalties == 1) {
          validatorRoyalties = 3;
          creatorMintNFT = false;
        } else {
          validatorRoyalties += 1;
        }
      } //events
    }
  } // unknownTransfers

  return MAIN_ACTIONS;
};

const getinfoTrx = async (nft, trxs) => {
  for (let trx_id = 0; trx_id < trxs.length; trx_id++) {
    let trx = trxs[trx_id];
    try {
      const response = await axios({
        method: 'get',
        url: `https://public-api.solscan.io/transaction/${trx.txHash}`,
        timeout: 200000 * 100,
      });
      if (response.data.tokenTransfers.length > 0) {
        const date = new Date(trx.blockTime * 1000).toISOString();
        if (CollectionTile != 'Invitation') {
          let royalties = await getRoyatilsAndDestination(
            nft.serial_number,
            response.data.signer[0],
            CollectionTile,
            response.data.unknownTransfers
          );

          let response_data = {
            nft_id: nft.id,
            nft_address: nft.contract_adress,
            signature: trx.txHash,
            date_trx: date,
            from_wallet: response.data.tokenTransfers[0].source_owner,
            toWallet: response.data.tokenTransfers[0].destination_owner,
            instructions: '',
            event: 'Spl-Transfer',
            price: 0,
          };

          let trx_temp = {};
          for (let i = 0; i < royalties.length; i++) {
            trx_temp = Object.assign({}, response_data, royalties[i]);
            response_data = trx_temp;
          }
          TRANFER_NFT.push(response_data);
        } else {
          let response_data = {
            nft_id: nft.id,
            nft_address: nft.contract_adress,
            signature: trx.txHash,
            date_trx: date,
            from_wallet: response.data.tokenTransfers[0].source_owner,
            toWallet: response.data.tokenTransfers[0].destination_owner,
            instructions: '',
            event: 'Spl-Transfer',
            price: 0,
          };
          TRANFER_NFT.push(response_data);
        }
      } // if tranfer
    } catch (error) {
      trx_id -= 1;
    } // end try-catch
  } // End For TRX

  // //Get Mint
  try {
    mint_hash = trxs[trxs.length - 1];
    const response = await axios({
      method: 'get',
      url: `https://public-api.solscan.io/transaction/${mint_hash.txHash}`,
      timeout: 200000 * 100,
    });
    const date = new Date(mint_hash.blockTime * 1000).toISOString();

    if (CollectionTile != 'Invitation') {
      let royalties = await getRoyatilsAndDestination(
        nft.serial_number,
        response.data.signer[0],
        CollectionTile,
        response.data.unknownTransfers
      );

      let response_data = {
        nft_id: nft.id,
        nft_address: nft.contract_adress,
        signature: mint_hash.txHash,
        date_trx: date,
        from_wallet: '',
        toWallet: response.data.unknownTransfers[0]['event'][0].source,
        instructions: '',
        event: 'Mint',
        price: 0,
      };

      let mint_data = {};
      for (let i = 0; i < royalties.length; i++) {
        mint_data = Object.assign({}, response_data, royalties[i]);
        response_data = mint_data;
      }
      TRANFER_NFT.push(response_data);
    } //end if
    else {
      let response_data = {
        nft_id: nft.id,
        nft_address: nft.contract_adress,
        signature: mint_hash.txHash,
        date_trx: date,
        from_wallet: '',
        toWallet: response.data.unknownTransfers[0]['event'][0].source,
        instructions: '',
        event: 'Mint',
        price: 0,
      };
      TRANFER_NFT.push(response_data);
    }
  } catch (e) {}

  if (TRANFER_NFT.length != 0) {
    try {
      await deleteTrxExists();
      if (TRANFER_NFT.length != 0) {
        await savenftDsTrx();
      }
    } catch (e) {
      console.log(e)
    }
  }
};

const nftTRX = async (nfts, nameCollection) => {
  CollectionTile = nameCollection;
  PoolData = await PoolDB();
  PoolData.push({
    title: 'Royalty',
    pool: '237izm5xHkaTVFXa76L9pLq2XyV4yJCF9LA3cCD5mBpf',
    wallet_adress: '7zP1BjWppJNpVj1ioxpKE9SaejxSismd8J7d3ortEcNn',
    symbol: 'RLTY',
  });
  let beforeHash = '';
  let trx_exits = true;
  for (let item = 0; item < nfts.length; item++) {
    let nft = nfts[item];
    while (trx_exits) {
      try {
        const response = await ApiSolana('/account/transactions', {
          params: {
            account: nft.contract_adress,
            beforeHash: beforeHash,
            limit: 50,
          },
          timeout: 10000 * 100,
        }); //end ApiSolana

        if (response.data.length == 0 || response == undefined)
          trx_exits = false;
        else {
          beforeHash = response.data[response.data.length - 1].txHash;
          if (TRX_SOL.length != 0) TRX_SOL = TRX_SOL.concat(response.data);
          else TRX_SOL = response.data;
        } // response.data.length > 1
      } catch (error) {}
    } //end while

    await getinfoTrx(nft, TRX_SOL);

    array_trx = [];
    trx_exits = true;
    beforeHash = '';
    TRX_SOL = [];
  } //end for
};

module.exports = nftTRX;
