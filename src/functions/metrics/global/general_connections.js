const { getAllActivitys } = require('../utils/index');

const { Users, NftTransactions, RequestInvitation } = require('../../../db');

const PoolData = [
  {
    title: 'Magic Spores',
    pool: '4QawWnjkTW91g3Qvh3cz39C9xQ4VQB6s181XB6xAe6hY',
    wallet_adress: 'CzCCZkzxGRaic54jmaMezmLxSudyQPTfRKvkVzet264f',
    symbol: '',
  },
  {
    title: 'Crypto Hills Club NFP',
    pool: 'Fp92aPit322vMts1NDuow1ZLmTDknuAUHqeQC4PmRFGT',
    wallet_adress: 'AMbQxtDNMxrMkUVb5fDaBKQFutvTS2SerfhG7DBgokaN',
    symbol: '',
  },
  {
    title: 'Royalty',
    pool: '237izm5xHkaTVFXa76L9pLq2XyV4yJCF9LA3cCD5mBpf',
    wallet_adress: '7zP1BjWppJNpVj1ioxpKE9SaejxSismd8J7d3ortEcNn',
    symbol: '',
  },
  {
    title: 'Zombie VS Infected',
    pool: 'GeCRaiFKTbFzBV1UWWFZHBd7kKcCDXZK61QvFpFLen66',
    wallet_adress: 'CGgz52xaPTzaTpb74Ja3ZA52xhRedE58d1xMAZ9FkwBM',
    symbol: '',
  },
  {
    title: 'vs Infected',
    pool: 'GeCRaiFKTbFzBV1UWWFZHBd7kKcCDXZK61QvFpFLen66',
    wallet_adress: 'CGgz52xaPTzaTpb74Ja3ZA52xhRedE58d1xMAZ9FkwBM',
    symbol: '',
  },
];

const saveMetricsNetwork = async (metricNetwork) => {
  for (let i = 0; i < metricNetwork.length; i++) {
    const {
      user_id = 'NULL',
      invitations_requested = 'NULL',
      invitation_sent = 'NULL',
      creator_royalties = 'NULL',
      connector_royalties = 'NULL',
      total_royalties = 'NULL',
    } = metricNetwork[i];
    const query = `INSERT INTO social_network (user_id, invitations_requested , invitation_sent, creator_royalties, connector_royalties ,total_royalties) VALUES (${user_id}, ${invitations_requested}, ${invitation_sent}, ${creator_royalties}, ${connector_royalties}, ${total_royalties});`;
    await process.postgresql.query(query);
  }
};

const royatilsConnector = async (users, metricNetwork, PoolData) => {
  let connector_royalties_count = 0;
  for (let i = 0; i < PoolData.length; i++) {
    let trxs_pool = await NftTransactions(PoolData[i].title);

    for (let k = 0; k < users.length; k++) {
      let connector_scout = trxs_pool.filter((trx_pool) => {
        return users[k].public_key == trx_pool.wallet_connector_scout;
      });

      for (let z = 0; z < metricNetwork.length; z++) {
        if (metricNetwork[z].user_id == users[k].id) {
          connector_scout.forEach((element) => {
            connector_royalties_count += element.connector_scout;
          });
          metricNetwork[z].connector_royalties = connector_royalties_count;
          connector_royalties_count = 0;
        }
      } //end metricNetwork
    } // end for users
  } //end for PoolData

  return metricNetwork;
}; // end royatilsConnector

const creatorRoyatils = async (users, metricNetwork, PoolData) => {
  let creator_royalties_count = 0;
  for (let i = 0; i < PoolData.length; i++) {
    let user_creator = users.find(
      (user) => user.public_key == PoolData[i].wallet_adress
    );
    if (user_creator) {
      let trxs_pool = await NftTransactions(PoolData[i].title);
      let creator_royalties = trxs_pool.filter((trx_pool) => {
        return user_creator.public_key == trx_pool.wallet_creator;
      });
      for (let k = 0; k < metricNetwork.length; k++) {
        if (metricNetwork[k].user_id == user_creator.id) {
          creator_royalties.forEach((element) => {
            creator_royalties_count += parseFloat(element.creator);
          });
          metricNetwork[k].creator_royalties = creator_royalties_count;
          creator_royalties_count = 0;
        }
      } //end metricNetwork
    } //end if user_creator
  } // for PoolData

  return metricNetwork;
};

const getAllRequestInvitation = async (
  users,
  activitys,
  request_invitation
) => {
  let userNetwork = [];
  let save_data = {};

  for (let i = 0; i < users.length; i++) {
    // Invitations Requested
    save_data.user_id = users[i].id;

    for (let k = 0; k < activitys.length; k++) {
      //by activitys
      if (activitys[k][0].type_action == 'userRequestInvitation') {
        save_data.invitations_requested = activitys[k].filter((element) => {
          return element.source_user_id == users[i].id;
        }).length;
      }
    }

    // RequestInvitation
    save_data.invitations_requested += request_invitation.filter(
      (element) => element.source_user_id == users[i].id
    ).length;

    userNetwork.push(save_data);
    save_data = {};
  }

  return userNetwork;
};

const getAllSendInvitations = async (
  users,
  invitatations_db,
  metricNetwork
) => {
  for (let i = 0; i < users.length; i++) {
    let user = users[i];
    let invitation_count = invitatations_db.filter(
      (invitation) =>
        invitation.from_wallet == user.public_key &&
        invitation.event == 'Spl-Transfer'
    ).length;

    for (let k = 0; k < metricNetwork.length; k++) {
      if (metricNetwork[k].user_id == user.id) {
        metricNetwork[k].invitation_sent = invitation_count;
      }
    }
  } //end for users

  return metricNetwork;
};

const scobyNetwork = async () => {
  //db consult
  const users = await Users();
  const activitys = await getAllActivitys();
  const request_invitation = await RequestInvitation();

  let metricNetwork = await getAllRequestInvitation(
    users,
    activitys,
    request_invitation
  );

  const invitatations_db = await NftTransactions('Invitation'); // obtener de la base de datos

  metricNetwork = await getAllSendInvitations(
    users,
    invitatations_db,
    metricNetwork
  );

  //metric creator
  metricNetwork = await creatorRoyatils(users, metricNetwork, PoolData);

  //metric connector
  metricNetwork = await royatilsConnector(users, metricNetwork, PoolData);

  // total royalties
  let creator_count = 0;
  let connector_royalties = 0;
  for (let i = 0; i < metricNetwork.length; i++) {
    if (metricNetwork[i]?.creator_royalties)
      creator_count = metricNetwork[i].creator_royalties;

    if (metricNetwork[i]?.connector_royalties)
      connector_royalties = metricNetwork[i].connector_royalties;

    metricNetwork[i].total_royalties = creator_count + connector_royalties;
    connector_royalties = 0;
    creator_count = 0;
  }

  console.log(metricNetwork);

  //await saveMetricsNetwork(metricNetwork)
};

module.exports = scobyNetwork;
