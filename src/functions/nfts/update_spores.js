const solana_web3 = require('@solana/web3.js');
const saves_nft = require('./get_save_spores');
let connection = new solana_web3.Connection(
  solana_web3.clusterApiUrl('mainnet-beta'),
  'confirmed'
);
const metaplex = require('@metaplex-foundation/mpl-token-metadata');
const axios = require('axios');
const sporesWeb = [];
const spore_fail = [];

const getMetadaToken = async (spore) => {
  const { contract_adress, wallet_adress = 'any' } = spore;
  let mintPubkey = new solana_web3.PublicKey(contract_adress);
  let tokenmetaPubkey = await metaplex.Metadata.getPDA(mintPubkey);
  const tokenmeta = await metaplex.Metadata.load(connection, tokenmetaPubkey);
  let meta = 'notEditable';
  if (tokenmeta.data.isMutable == 1) {
    meta = 'Editable';
  }

  sporesWeb.push({
    creator: tokenmeta.data.data.creators[2].address,
    metadada: meta,
  });
};

const updateSpores = async () => {
  let spores_orphans = await saves_nft();
  let values = [];
  for (let item = 0; item < spores_orphans.length; item++) {
    let spore = spores_orphans[item];
    await getMetadaToken(spore);
  }

  for (let i = 0; i < spores_orphans.length; i++) {
    let spore = spores_orphans[i];
    let data_r = sporesWeb[i];
    const query = `UPDATE spore_ds SET "web_site"="https://www.mapshifting.com/magic_spore", "block_chain"="solana", "type_collections"="Magic Spores" "creator"="${data_r.creator}", "meta_data"="${data_r.metadada}" WHERE "id"="${spore.id}";`;
    await process.postgresql.query(query);
  }
};

module.exports = updateSpores;
