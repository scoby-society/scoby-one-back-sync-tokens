const solana_web3 = require('@solana/web3.js');
const connection = new solana_web3.Connection(
  solana_web3.clusterApiUrl('mainnet-beta'),
  'confirmed'
);
const metaplex = require('@metaplex-foundation/mpl-token-metadata');
const axios = require('axios');

const CREATORS = [
  'CzCCZkzxGRaic54jmaMezmLxSudyQPTfRKvkVzet264f',
  'BhfqmcMMEbf5gRJ4hwXzmso7BiBYztpnuqTjzXNt9sEe',
];
const getMetadaToken = async (name_collections, nfts) => {
  let nftsWeb = [];
  let nft_fail = [];
  let serial_number = '';
  let effect = '';
  let creator = '';
  let website = '';
  let name = '';
  for (let id = 0; id < nfts.length; id++) {
    let nft = nfts[id];
    const { contract_adress, wallet_adress = 'any' } = nft;
    let mintPubkey = new solana_web3.PublicKey(contract_adress);
    let tokenmetaPubkey = await metaplex.Metadata.getPDA(mintPubkey);
    const tokenmeta = await metaplex.Metadata.load(connection, tokenmetaPubkey);
    let meta = 'notEditable';
    if (tokenmeta.data.isMutable == 1) meta = 'Editable';
    try {
      let metadata = await axios({
        method: 'get',
        url: tokenmeta.data.data.uri,
        timeout: 10000 * 100,
      });

      if (name_collections == 'Magic Spores') {
        creator = CREATORS[0];
        serial_number = metadata.data.edition;
      } else {
        creator = CREATORS[1];
        serial_number = Number(tokenmeta.data.data.name.split('#')[1]);
      }

      try {
        effect = metadata.data.attributes[1].value;
      } catch (error) {
        effect = null;
      }

      //website
      if (name_collections == 'Invitation') {
        website = 'https://scoby.one/projects/invitation';
      } else if (name_collections == 'Crypto Hills Club NFP') {
        website = 'https://thecryptohillsclub.godaddysites.com';
      } else {
        website = metadata.data.external_url;
      }

      nftsWeb.push({
        name: tokenmeta.data.data.name,
        symbol: metadata.data.symbol,
        description: metadata.data.description,
        website: website,
        background: metadata.data.attributes[0].value,
        metadata: meta,
        tokenId: nft.contract_adress,
        creator: creator,
        type_collections: nft.type_collections,
        blockchain: 'solana',
        effect: effect,
        contract_adress: nft.contract_adress,
        wallet_adress: wallet_adress,
        serial_number: serial_number,
        uri: tokenmeta.data.data.uri,
        url_image: metadata.data.image,
        user_id: nft.user_id,
      });
    } catch (error) {
      nft_fail.push({
        uri: tokenmeta.data.data.uri,
        contract_adress: contract_adress,
        wallet_adress: wallet_adress,
      });
    } //EndCatch
  } //EndFor

  return {
    nfts_data: nftsWeb,
    nfts_fail: nft_fail,
  };
};

module.exports = getMetadaToken;
