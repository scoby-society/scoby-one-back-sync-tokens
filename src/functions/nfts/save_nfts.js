const saveNfts = async (nft_orphans) => {
  if (nft_orphans.length != 0) {
    for (let i = 0; i < nft_orphans.length; i++) {
      let nft = nft_orphans[i];
      const {
        name = 'NULL',
        symbol = 'NULL',
        description = 'NULL',
        website = 'NULL',
        background = 'NULL',
        metadata = 'NULL',
        blockchain = 'NULL',
        effect = 'NULL',
        wallet_adress = 'NULL',
        contract_adress = 'NULL',
        serial_number = 'NULL',
        uri = 'NULL',
        url_image = 'NULL',
        user_id = 'NULL',
        type_collections = 'NULL',
        tokenId = 'NULL',
        creator = 'NULL',
      } = nft;
      const query = `INSERT INTO spore_ds (name, symbol, description, website, background, metadata, blockchain, effect, wallet_adress, contract_adress, serial_number, uri, url_image, user_id, type_collections, tokenId, creator) VALUES (${name}, ${symbol}, ${description}, ${website}, ${background}, ${metadata}, ${blockchain}, ${effect}, ${wallet_adress}, ${contract_adress}, ${serial_number}, ${uri}, ${url_image}, ${user_id}, ${type_collections}, ${tokenId}, ${creator});`;
      await process.postgresql.query(query);
    }
  }
  await pool.end();
};

module.exports = saveNfts;
