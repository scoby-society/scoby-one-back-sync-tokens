const getMetadaToken = require('./get_metada_token');
/*
    Sincronizar usuarios a nfts utilizando la wallet de minteo,
    obtener metada de los nfts
*/

const syncNfts = async (name_collections, nft_collections, nft_orphans) => {
  let data = {};
  data = await getMetadaToken(name_collections, nft_orphans);
  return data;
};

const getFailMetada = async (fail_array) => {
  let sucess = true;
  let data = {};
  let intentos = 0;
  let response = [];

  while (sucess) {
    intentos += 1;
    data = await getMetadaToken(fail_array);
    if (data.nfts_data.length != 0) {
      if (response.length != 0) {
        response.concat(data.nfts_data);
      } else {
        response = data.nfts_data;
      }
    }

    if (data.nfts_fail.length == 0) sucess == false;
  }
  return [];
};

module.exports = {
  syncNfts: syncNfts,
  getFailMetada: getFailMetada,
};
