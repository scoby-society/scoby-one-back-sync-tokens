const axios = require('axios');

const { apiSolana } = require('../../config');

const ApiSolana = axios.create({
  baseURL: apiSolana,
  headers: {
    'Content-Type': 'application/json',
  },
});

const getUnknownTrx = async (txHash) => {
  try {
    const response = await ApiSolana(`/transaction/${txHash}`);
    if (response.data.tokenBalanes.length == 0) {
      return '';
    } else {
      return response.data.tokenBalanes[0].token.tokenAddress;
    }
  } catch (error) {}
};

const obtainNftOrphans = async (
  name_collections,
  nft_collection,
  poolAdress
) => {
  let contract_adress = [];
  let nft_trxs = [];
  if (nft_collection.length != 0) {
    nft_collection.forEach((element) => {
      contract_adress.push(element.contract_adress);
    });
  }

  let is_spores = true;
  let last_trx = '';
  let temp_array = [];
  let beforeHash = '';

  while (is_spores) {
    const response = await ApiSolana('/account/transactions', {
      params: {
        account: poolAdress,
        beforeHash,
        limit: 50,
      },
    }); //end ApiSolana

    if (response.data.length == 0 || response == undefined) is_spores = false;
    else {
      last_trx = response.data[response.data.length - 1];
      beforeHash = last_trx.txHash;

      //delete trx with spore exists
      for (let item = 0; item < response.data.length; item++) {
        let trx = response.data[item];
        if (trx.parsedInstruction[0].type != 'Unknown') {
          if (contract_adress.indexOf(trx.signer[1]) == -1) {
            nft_trxs.push({
              wallet_adress: trx.signer[0],
              contract_adress: trx.signer[1],
              user: null,
              type_collections: name_collections,
            });
          }
        } else {
          let contractAdress = await getUnknownTrx(trx.txHash);
          if (contractAdress != '') {
            if (contract_adress.indexOf(contractAdress) == -1) {
              nft_trxs.push({
                wallet_adress: trx.signer[0],
                contract_adress: contractAdress,
                user: null,
                type_collections: name_collections,
              });
            }
          }
        }
      } // end for
    }
  } //end while

  return nft_trxs;
};

module.exports = obtainNftOrphans;
