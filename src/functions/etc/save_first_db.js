const solana_web3 = require('@solana/web3.js');
const metaplex = require('@metaplex-foundation/mpl-token-metadata');
const axios = require('axios');

const SporeDB = require('../nfts/get_save_spores');

let connection = new solana_web3.Connection(
  solana_web3.clusterApiUrl('mainnet-beta'),
  'confirmed'
);

const sporesWeb = [];
const sporeFail = [];

const getMetadaToken = async (spore) => {
  const { contract_adress, wallet_adress = 'any' } = spore;
  let mintPubkey = new solana_web3.PublicKey(contract_adress);
  let tokenmetaPubkey = await metaplex.Metadata.getPDA(mintPubkey);
  const tokenmeta = await metaplex.Metadata.load(connection, tokenmetaPubkey);
  let meta = 'notEditable';
  if (tokenmeta.data.isMutable) {
    meta = 'Editable';
  }
  try {
    let metadata = await axios({
      method: 'get',
      url: tokenmeta.data.data.uri,
      timeout: 1000 * 100,
    });
    sporesWeb.push({
      name: metadata.data.name,
      symbol: metadata.data.symbol,
      description: metadata.data.description,
      website: metadata.data.external_url,
      background: metadata.data.attributes[0].value,
      metadata: meta,
      tokenId: '',
      creator: tokenmeta.data.data.creators[2],
      blockchain: 'solana',
      effect: metadata.data.attributes[1].value,
      contract_adress: spore.contract_adress,
      wallet_adress: wallet_adress,
      serial_number: metadata.data.edition,
      uri: tokenmeta.data.data.uri,
      url_image: metadata.data.image,
      user_id: spore.user_id,
    });
  } catch (error) {
    sporeFail.push({
      uri: tokenmeta.data.data.uri,
      contract_adress: contract_adress,
      wallet_adress: wallet_adress,
    });
  }
};

const saveSporeFirst = async () => {
  let values = [];
  let spores = await SporeDB();
  console.log(spores.length);

  // for (let i = 0; i < spores.length; i++) {
  //   let spore = spores[i];
  // }

  // for (let k = 0; k < sporeFail.length; k++) {
  //   let spore_2 = sporeFail[k];
  // }

  for (let i = 0; i < spores.length; i++) {
    let spore = spores[i];
    let {
      name,
      symbol,
      description,
      website,
      background,
      metadata,
      blockchain,
      effect,
      wallet_adress,
      contract_adress,
      serial_number,
      uri,
      url_image,
      user_id = null,
    } = spore;
    const query = `INSERT INTO spore_ds (name, symbol, description, website, background, meta_data, block_chain, effect, wallet_adress, contract_adress, serial_number, uri, url_image, user_id) VALUES (${name}, ${symbol}, ${description}, ${website}, ${background}, ${metadata}, ${blockchain}, ${effect}, ${wallet_adress}, ${contract_adress}, ${serial_number}, ${uri}, ${url_image}, ${
      user_id ? user_id : 'NULL'
    })`;
    await process.postgresql.query(query);
  }
};

saveSporeFirst()
  .then((res) => {
    console.log('sucess');
  })
  .catch((err) => console.log(err));
