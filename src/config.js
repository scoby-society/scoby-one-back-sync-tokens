module.exports = {
  url: process.env.SCOBY_UR,
  apiSolana: process.env.SOLANA_API_URL,
  databaseUser: process.env.DATABASE_USER,
  databaseName: process.env.DATABASE_NAME,
  databasePassword: process.env.DATABASE_PASSWORD,
  databaseHost: process.env.DATABASE_HOST,
  databasePort: process.env.DATABASE_PORT,
};
