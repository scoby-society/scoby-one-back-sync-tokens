require('dotenv').config();
const { postgresql } = require('./db/postgres');
postgresql();

//get DB
const { PoolDB, GetNftCollection } = require('./db');

//for nfts
const obtainNftOrphans = require('./functions/nfts/obtain_nft_orphans');
const { syncNfts } = require('./functions/nfts/sync_nfts');
const saveNfts = require('./functions/nfts/save_nfts');
const syncNftsUsers = require('./functions/users/sync_nft_user');

//metrics
const scobyNetwork = require('./functions/metrics/global/general_connections');
const MembersNetwork = require('./functions/metrics/members/member_network');

//trx
const nftTRX = require('./functions/metrics/utils/creator_royatils');

const getCurrentTime = () => {
  const dt = new Date();
  const padToDigits = (num, digits) => num.toString().padStart(digits, '0');
  const padTo2Digits = (num) => padToDigits(num, 2);
  const padTo4Digits = (num) => padToDigits(num, 4);
  return `${padTo2Digits(dt.getDate())}/${padTo2Digits(
    dt.getMonth() + 1
  )}/${padTo4Digits(dt.getFullYear())} ${padTo2Digits(
    dt.getHours()
  )}:${padTo2Digits(dt.getMinutes())}:${padTo2Digits(dt.getSeconds())}`;
};

const syncNFTs = async () => {
  console.log('syncNFTs', getCurrentTime());
  let nft_collections = [],
    nft_orphans = [],
    nft_finaly = [],
    save_nfts = [],
    poolCollections = [];

  // Get nfts save in DB
  poolCollections = await PoolDB();
  poolCollections.push({
    title: 'Royalty',
    pool: '237izm5xHkaTVFXa76L9pLq2XyV4yJCF9LA3cCD5mBpf',
    wallet_adress: '7zP1BjWppJNpVj1ioxpKE9SaejxSismd8J7d3ortEcNn',
    symbol: 'RLTY',
  });

  for (let i = 0; i < poolCollections.length; i++) {
    nft_collections.push(GetNftCollection(poolCollections[i].symbol));
  }
  nft_collections = await Promise.all(nft_collections);

  //Get NFT Orphans
  for (let i = 0; i < nft_collections.length; i++) {
    let _nft_orphans = [];
    //if (nft_collections[i]?.length)
    try {
      _nft_orphans = obtainNftOrphans(
        poolCollections[i].title,
        nft_collections[i],
        poolCollections[i].pool
      );
      nft_orphans.push(_nft_orphans);
    } catch (error) {}
  }
  nft_orphans = await Promise.all(nft_orphans);
  for (let i = 0; i < nft_collections.length; i++) {
    let _nft_finaly;
    try {
      if (nft_orphans[i]?.length) {
        _nft_finaly = syncNfts(
          poolCollections[i].title,
          nft_collections[i],
          nft_orphans[i]
        );
        nft_finaly.push(_nft_finaly);
      }
    } catch (error) {}
  }

  nft_finaly = await Promise.all(nft_finaly);

  for (let i = 0; i < nft_collections.length; i++) {
    let _save_nfts;
    try {
      if (nft_finaly[i].nfts_data?.length) {
        _save_nfts = saveNfts(nft_finaly[i].nfts_data);
      }
      save_nfts.push(_save_nfts);
    } catch (e) {}
  }
  save_nfts = await Promise.all(save_nfts);
};

const syncUsers = async () => {
  console.log('syncUsers', getCurrentTime());
  await syncNftsUsers();
};

const addTrxNFT = async () => {
  console.log('addTrxNFT', getCurrentTime());
  // Get nfts save in DB
  let nft_collections = [];
  poolCollections = await PoolDB();
  poolCollections.push({
    title: 'Royalty',
    pool: '237izm5xHkaTVFXa76L9pLq2XyV4yJCF9LA3cCD5mBpf',
    wallet_adress: '7zP1BjWppJNpVj1ioxpKE9SaejxSismd8J7d3ortEcNn',
    symbol: 'RLTY',
  });

  for (let i = 0; i < poolCollections.length; i++) {
    nft_collections.push(GetNftCollection(poolCollections[i].symbol));
  }

  nft_collections = await Promise.all(nft_collections);
  for (let i = 0; i < nft_collections.length; i++) {
    await nftTRX(nft_collections[i], poolCollections[i].title);
  }
};

setInterval(async () => await syncNFTs(), 20 * 60 * 1000); // 20mins
setInterval(async () => await syncUsers(), 30 * 60 * 1000); // 30mins
setInterval(async () => await addTrxNFT(), 35 * 60 * 1000); // 35mins

// sync_nfts().then((resp) => {
//     console.log("Probando")
// })

// sysnc_users().then((rep) => {
//     console.log("E")
// })

//addTrxNFT().then((rep) => { console.log("ss") })

// scobyNetwork().then((resp) => {
//     console.log("Metrics Social")
// })

// MembersNetwork().then((resp)=>{
//     console.log("hola")
// })
