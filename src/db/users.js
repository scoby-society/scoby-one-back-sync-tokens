const Users = async () => {
  const query =
    'SELECT id, created_at, updated_at, phone, public_key, verified_public_key, verification_limit, registration_status_id, username FROM public.user WHERE verified_public_key=true;';
  const rows = await process.postgresql.query(query);
  return rows;
};

module.exports = { Users };
