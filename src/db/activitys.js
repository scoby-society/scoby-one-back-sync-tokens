//get connections of db
const Activitys = async () => {
  const query =
    'SELECT id, created_at, type_action, source_user_id, target_user_id FROM public.activity;';
  const rows = await process.postgresql.query(query);
  return rows;
};

//get Request Invitation
const RequestInvitation = async () => {
  const query = 'SELECT * FROM public.request_invitation;';
  const rows = await process.postgresql.query(query);
  return rows;
};

module.exports = {
  Activitys,
  RequestInvitation,
};
