const PoolDB = async () => {
  const query = 'SELECT symbol, title, pool, wallet_adress FROM public.pool;';
  const rows = await process.postgresql.query(query);
  return rows;
};

module.exports = { PoolDB };
