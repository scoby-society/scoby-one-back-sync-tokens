const NftTransactions = async (name) => {
  const query = `SELECT * FROM public.nfts_transactions WHERE source_collection='${name}';`;
  const rows = await process.postgresql.query(query);
  return rows;
};

module.exports = { NftTransactions };
