const { Pool } = require('pg');

const config = {
  host: process.env.DATABASE_HOST,
  user: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE_NAME,
  port: process.env.DATABASE_PORT,
};

const postgresql = (callback = null) => {
  const pool = new Pool(config);

  const connection = {
    pool,
    query: async (...args) => {
      return pool.connect().then((client) => {
        console.log(...args);
        return client.query(...args).then((res) => {
          client.release();
          return res.rows;
        });
      });
    },
  };

  process.postgresql = connection;

  if (callback) {
    callback(connection);
  }

  return connection;
};

module.exports = { postgresql };
